import exchanges.BinanceFutures as binfut
import exchanges.Bybit as bybit
import exchanges.Bitmex as bitmex
import Plotter as plotter
import pickle


challenger_data = {
    "EXAMPLE": {
        "exchange": bitmex,
        "key": "",
        "secret": "",
        "start_date": "2020-09-01",
        'chart_options': {
            'chart_color': 'MediumPurple',
            'account_name': 'NAME'
        }
    }
}

# sh_acc_data = binfut.getAccountInformation(challenger_data['NAME'])
# plotter.single_plot_picture(sh_acc_data, challenger_data['NAME']['chart_options'])

# sh_acc_data_bbit = bybit.getAccountInformation(challenger_data['NAMEByBit'])
# plotter.single_plot_picture(sh_acc_data_bbit, challenger_data['NAMEByBit']['chart_options'])

# sh_acc_data_bm = bitmex.getAccountInformation(challenger_data['NAMEMex'])
# plotter.single_plot_picture(sh_acc_data_bm, challenger_data['NAMEMex']['chart_options'])

all_data = []
for challenger in challenger_data.values():
    all_data.append(dict({'data': challenger['exchange'].getAccountInformation(challenger), 'chart_options': challenger['chart_options']}))

plotter.plot_all_challengers(all_data)