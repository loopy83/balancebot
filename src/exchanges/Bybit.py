from datetime import datetime
import time
import requests
import ctypes
import functools
import pickle
import pandas as pd
import hmac
import json
import time


BASE_URL = 'https://api.bybit.com'
BALANCE = '/v2/private/wallet/balance'
PNL = '/v2/private/trade/closed-pnl/list'

def get_signature(secret: str, req_params: dict):
    """
    :param secret    : str, your api-secret
    :param req_params: dict, your request params
    :return: signature
    """
    _val = '&'.join([str(k)+"="+str(v) for k, v in sorted(req_params.items()) if (k != 'sign') and (v is not None)])
    return str(hmac.new(bytes(secret, "utf-8"), bytes(_val, "utf-8"), digestmod="sha256").hexdigest())

def getStart():
    return int(datetime(year=2020, month=9, day=1, hour=2).timestamp() * 1000)


def getPnlData(key, secret, symbol):
    params = dict()
    params['api_key'] = key
    params['symbol'] = symbol
    params['timestamp'] = str(int(round(time.time())-1))+"000"
    params['sign'] = get_signature(secret, params)
    
    from_time = getStart() / 1000
    endTime = str(int(round(time.time())-1))+"000"

    pnls = []
    pnls.append(dict({
        'timestamp': getStart() / 1000,
        'balance': 0,
        'realizedPnl': 0,
        'total_pnl': 0,
        'delta_to_start': 0,
    }))

    known_order_ids = []
    while(int(from_time) < int(endTime)):
        data = json.loads(requests.get(BASE_URL + PNL, params).text)
        data_array = list(filter(lambda x: x['id'] not in known_order_ids, data['result']['data']))
        if len(data_array):
            for entry in data['result']['data']:
                known_order_ids.append(entry['id'])
                pnls.append(dict({
                    'timestamp': entry['created_at'],
                    'realizedPnl': entry['closed_pnl'],
                }))
                from_time = entry['created_at'] + 1
        else:
            break
        time.sleep(1)

    return pnls

def getTotalBalance(key, secret):
    params = dict()
    params['api_key'] = key
    params['timestamp'] = str(int(round(time.time())-1))+"000"
    params['sign'] = get_signature(secret, params)
    return json.loads(requests.get(BASE_URL + BALANCE, params = params).text)['result']['BTC']['equity']

def getDelta(first, second):
    diff = second - first
    return diff / first * 100


def getAccountInformation(loginData: dict = None):

    profits_and_losses = getPnlData(loginData['key'], loginData['secret'], 'BTCUSD')
    current_balance = getTotalBalance(loginData['key'], loginData['secret'])

    total_pnl = functools.reduce(lambda prev, value: prev + float(value['realizedPnl']), profits_and_losses, 0.0)
    starting_balance = current_balance - total_pnl

    pnls = sorted(list(map(lambda item: dict({'realizedPnl': float(item['realizedPnl']), 'timestamp': item['timestamp']}), profits_and_losses)), key=lambda k: k['timestamp'])

    pnl_series = []
    pnl_series.append(dict({
        'timestamp': getStart() / 1000,
        'balance': starting_balance,
        'realizedPnl': 0,
        'total_pnl': 0,
        'delta_to_start': 0,
    }))

    for pnl in pnls:
        entry = dict({
            'timestamp': pnl['timestamp'],
            'balance': (pnl_series[-1]['balance'] + pnl['realizedPnl']),
            'realizedPnl': pnl['realizedPnl'],
            'total_pnl': (pnl_series[-1]['total_pnl'] + pnl['realizedPnl']),
            'delta_to_start': getDelta(starting_balance, starting_balance + (pnl_series[-1]['total_pnl'] + pnl['realizedPnl'])),
        })
        pnl_series.append(entry)


    if len(pnl_series) > 0:
        pnl_series.append({
            'timestamp': str(int(round(time.time())-1)),
            'balance': pnl_series[-1]['balance'],
            'realizedPnl': 0,
            'total_pnl': pnl_series[-1]['total_pnl'],
            'delta_to_start': pnl_series[-1]['delta_to_start']
        })

    pnl_df = pd.DataFrame(pnl_series)
    pnl_df['timestamp'] = pd.to_datetime(pnl_df.timestamp, unit='s')

    balance_data = {
        'starting_balance': starting_balance,
        'current_balance': current_balance,
        'pnl_df': pnl_df
    }


    return balance_data
