from datetime import datetime
import ctypes
import ccxt
import functools
import pickle
import pandas as pd

def getStart():
    return datetime(year=2020, month=9, day=1, hour=0).timestamp() * 1000

def getStartInStamp():
    return datetime(year=2020, month=9, day=1, hour=0)

def init_ccxt(key, secret) -> ccxt.Exchange:
    try:
        api = getattr(ccxt, 'bitmex')({
            'apiKey': key,
            'secret': secret,
            'enableRateLimit': True,
            'rateLimit': 2000,
        })

    except (KeyError, AttributeError) as error:
        print(error)

    return api


def getDelta(first, second):
    diff = second - first
    return diff / first * 100


def getAllMyTrades(ccxtApi):
    # symbols = [item['symbol'] for item in ccxtApi.fetchMarkets()]
    all_trades = dict({})
    starting = getStart()
    # for sym in symbols:
        # print(sym)

    # test = ccxtApi.fetch_ledger(since=ccxtApi.iso8601(starting), limit=200)
    # print(test)

    while(starting < ccxtApi.milliseconds()):
        symbol_trade_history = ccxtApi.fetch_ledger(since=ccxtApi.iso8601(starting), limit= 200)
        if len(symbol_trade_history):
            starting = symbol_trade_history[len(symbol_trade_history) - 1]['timestamp'] + 1
            for trade in symbol_trade_history:
                if(trade['status'] == "ok" and getStart() < trade['timestamp']):
                    all_trades[str(trade['timestamp']) + '_' + str(trade['info']['transactID'])] = trade['info']
        else:
            break
    return all_trades


def getAccountInformation(loginData: dict = None):

    ccxtApi = init_ccxt(loginData['key'], loginData['secret'])
    all_trades = getAllMyTrades(ccxtApi)

    total_pnl = functools.reduce(lambda prev, value: prev + float(value['amount'] if value['transactType'] != "Deposit" else 0), all_trades.values(), 0.0)
    pnls = sorted(list(map(lambda item: dict({'realizedPnl': float(item['amount']), 'timestamp': item['transactTime']}), all_trades.values())), key=lambda k: k['timestamp'])
    current_balance = int(ccxtApi.fetchBalance()['info'][0]['walletBalance'])

    starting_balance = current_balance - total_pnl

    pnl_series = []
    pnl_series.append(dict({
        'timestamp': getStartInStamp(),
        'balance': starting_balance,
        'realizedPnl': 0,
        'total_pnl': 0,
        'delta_to_start': 0,
    }))
    for pnl in pnls:
        entry = dict({
            'timestamp': pnl['timestamp'],
            'balance': (pnl_series[-1]['balance'] + pnl['realizedPnl']),
            'realizedPnl': pnl['realizedPnl'],
            'total_pnl': (pnl_series[-1]['total_pnl'] + pnl['realizedPnl']),
            'delta_to_start': getDelta(starting_balance, starting_balance + (pnl_series[-1]['total_pnl'] + pnl['realizedPnl'])),
        })
        pnl_series.append(entry)

    if len(pnl_series) > 0:
        pnl_series.append({
            'timestamp': datetime.now(),
            'balance': pnl_series[-1]['balance'],
            'realizedPnl': 0,
            'total_pnl': pnl_series[-1]['total_pnl'],
            'delta_to_start': pnl_series[-1]['delta_to_start']
        })

    pnl_df = pd.DataFrame(pnl_series)
    pnl_df['timestamp'] = pd.to_datetime(pnl_df.timestamp)

    balance_data = {
        'starting_balance': starting_balance,
        'current_balance': current_balance,
        'pnl_df': pnl_df
    }


    return balance_data
