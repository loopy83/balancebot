import discord

from discord.ext import commands
from Runner import generate_image
from Runner import generate_image_single_user
from Runner import update_challenger_data
from Runner import get_account_for_user
import Runner


TOKEN = 'NzI2NTg4OTU2NzU3NTkwMDY2.Xvfesg.QFODhvXbHdbx5KdV_pUGAiFHJnI'

client = commands.Bot(command_prefix = '!')

@client.event
async def on_ready():
    print("Logged in!")
    Runner.loadData()

@client.command()
async def stop(ctx):
    if(ctx.message.author.id == 277621329430183937):
        await client.logout()
    else:
        await ctx.send('Ah ah ah! Not allowed to do that!')

@client.command()
async def get_standing(ctx):
    await ctx.send('Generating Image.... this could take a few')
    image_filename = generate_image()
    await ctx.send(file=discord.File(image_filename))


async def sendHelpInfo(ctx):
    embed=discord.Embed(title="Help", description="Hier sind die benötigten Befehle um deine Daten zu Aktualisieren /n These are the necessary commands you can use to update your data.", color=0xa02727)
    embed.set_author(name="SkyChanger#0752")
    embed.add_field(name="!enter exchange", value="bybit|bitmex|binance", inline=False)
    embed.add_field(name="!enter api_key", value="API_KEY", inline=False)
    embed.add_field(name="!enter secret_key", value="SECRET_KEY", inline=False)
    embed.add_field(name="!enter chart_color", value="https://www.quackit.com/css/color/charts/css_color_names_chart.cfm", inline=False)
    embed.add_field(name="!enter account_name", value="NAME", inline=False)
    embed.add_field(name="!get_account", value="Returns your currently saved Account Data", inline=False)
    embed.set_footer(text="This bot certainly ain't the safest thing in the World. Just Saying. I don't take responsibility if something breaks. The API keys should be restircted!")
    await ctx.message.author.send(embed=embed)

client.remove_command('help')

@client.command()
async def help(ctx):
    await sendHelpInfo(ctx)

@client.command()
async def info(ctx):
    await sendHelpInfo(ctx)

@client.command()
async def enter(ctx, field, value):
    userId = str(ctx.message.author.id)
    if(ctx.message.channel.type == discord.enums.ChannelType.private):
        update_challenger_data(userId, field, value)
        await ctx.message.author.send(f'Saved new value for {field}')


@client.command()
async def get_account(ctx):
    userId = str(ctx.message.author.id)
    if(ctx.message.channel.type == discord.enums.ChannelType.private):
        await ctx.message.author.send(f'```{get_account_for_user(userId)}```')

@client.command()
async def my_chart(ctx):
    userId = str(ctx.message.author.id)
    if(ctx.message.channel.type == discord.enums.ChannelType.private):
        await ctx.message.author.send('Generating Image.... this could take a bit')
        await ctx.message.author.send(file=discord.File(generate_image_single_user(userId)))

client.run(TOKEN)